// app/router/index.js
import Vue from 'nativescript-vue';
import NSVueRouter from 'nativescript-vue-router-ns';
Vue.use(NSVueRouter);

const routes = [
   {
      name: 'home',
      path: '/home',
      meta: { guest: true },
      component: () => import('@/views/Home.vue')
   },
   {
      name: 'cart',
      path: '/cart',
      meta: { guest: true },
      component: () => import('@/views/Cart.vue')
   },
];

const router = new NSVueRouter({
   routes,
   verbose: false
});

export default router;