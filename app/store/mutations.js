export default {
    SET_USER: (state, payload) => {
        state.user = payload;
    },
    SET_TEXT: (state, payload) => {
        state.text = payload;
    },
    SET_CATEGORIES: (state, payload) => {
        state.categories = payload;
    },
    SET_CHECKOUT: (state, payload) => {
        state.checkout = payload;
    },
    SET_MENU: (state, payload) => {
        state.menu = payload;
    },
    SET_RULES: (state, payload) => {
        state.rules = payload;
    },
}