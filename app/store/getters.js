// https://vuex.vuejs.org/en/getters.html

export default {
    USER: state => {
        return state.user;
    }
}