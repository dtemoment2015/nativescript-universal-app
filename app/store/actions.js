export default {
    setUser({ commit }, payload) {
        commit('SET_USER', payload);
    },
    setCategories({ commit }, payload) {
        commit('SET_CATEGORIES', payload);
    },
    setText({ commit }, payload) {
        commit('SET_TEXT', payload);
    },
    setCheckout({ commit }, payload) {
        commit('SET_CHECKOUT', payload);
    },
    setMenu({ commit }, payload) {
        commit('SET_MENU', payload);
    },
    setRules({ commit }, payload) {
        commit('SET_RULES', payload);
    },
}
