export default {
    user: null,
    categories: [],
    text: {},
    checkout: {},
    menu: {},
    rules: {},
    bottom_bar: [
        {
            key: 'home-page',
            title: 'home',
            ico: '\ue921',
            badge: 0
        },
        {
            key: 'post-page',
            title: 'post',
            ico: '\ue911',
            badge: 0
        },
        {
            key: 'wish-page',
            title: 'desire',
            ico: '\ue901',
            badge: 3
        },
        {
            key: 'cart-page',
            title: 'basket',
            ico: '\ue909',
            badge: 1
        },
        {
            key: 'profile-page',
            title: 'account',
            ico: '\ue90a',
            badge: 2
        },
    ]
}