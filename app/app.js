import Vue from 'nativescript-vue';
import MainPage from '@/views/MainPage';
import store from '@/store';

new Vue({
  store,
  render: (h) => h('frame', [h(MainPage)]),
}).$start()