import { Http } from '@nativescript/core'
import store from '@/store/index';

export default async function apiStorage() {
   return await Http.request({
      url: 'https://api.arenamarkaz.uz/api/client/v1/web',
      method: 'GET',
      headers: { "Content-Type": "application/json" },
   }).then(
      (response) => {
         let data = JSON.parse(response.content);
         store.dispatch('setText', data.text);
         store.dispatch('setCategories', data.categories);
         store.dispatch('setRules', data.rules);
         store.dispatch('setMenu', data.menu);
         store.dispatch('setCheckout', data.checkout);
      },
      e => {
         return null;
      }
   ).finally(() => {

   })
}